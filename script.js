/**
 * Created on 13.08.2019.
 */

/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {
    constructor(size, stuffing){
        this._size = size;
        this._stuffing  = stuffing;
        this._toppings = [];
        try{
            if(size.size!=='SIZE_SMALL'&& size.size!=='SIZE_LARGE'){
                throw new HamburgerException('enter correct size')
            }
            if(stuffing.stuffing!=='STUFFING_CHEESE'&& stuffing.stuffing!=='STUFFING_SALAD'&& stuffing.stuffing!=='STUFFING_POTATO'){
                throw new HamburgerException('enter correct stuffing')
            }
        }
        catch(err){
            console.log('enter correct data, error:', err);
        }
    }
    static SIZE_SMALL = {
        size: 'SIZE_SMALL',
        price: 50,
        kcal: 20
    };
    static SIZE_LARGE = {
        size: 'SIZE_LARGE',
        price: 100,
        kcal:40
    };
    static STUFFING_CHEESE = {
        stuffing: 'STUFFING_CHEESE',
        price: 10,
        kcal: 20
    };
    static STUFFING_SALAD = {
        stuffing: 'STUFFING_SALAD',
        price: 20,
        kcal: 5
    };
    static STUFFING_POTATO = {
        stuffing:'STUFFING_POTATO',
        price: 15,
        kcal: 10
    };
    static TOPPING_MAYO = {
        topping: 'TOPPING_MAYO',
        price: 20,
        kcal: 5
    };
    static TOPPING_SPICE = {
        topping: 'TOPPING_SPICE',
        price: 15,
        kcal: 0
    };
    addTopping (topping) {
        try{
            if(topping.topping!=='TOPPING_MAYO' && topping.topping!=='TOPPING_SPICE'){
            throw new HamburgerException('Please, add correct topping!');
            }
            let match = false;
            this._toppings.forEach((el)=>{
                if(topping===el){
                match = true;
                }
            });
            if(!match){
                this._toppings.push(topping);
                console.log(`New topping ${topping.topping} was added`)
            }else{
                throw new HamburgerException('Already have this topping!');
            }
        }
        catch(err){
            console.log('enter correct data, error:', err);
        }
    };
    removeTopping (topping) {
        try{
            let match = false;
            this._toppings.forEach((el)=>{
                if(topping===el){
                match = true;
                }
            });
            if(!match){
                throw new HamburgerException(`Topping ${topping.topping} wasn't added!`);
            }else{
                this._toppings.splice(this._toppings.indexOf(topping),1);
                console.log(`Topping ${topping.topping} was removed`)
            }
        }
        catch(err){
            console.log('enter correct data, error:', err);
        }
    };
    getToppings (){
        return this._toppings;
    };
    getSize (){
        return this._size;
    };
    getStuffing () {
        return this._stuffing;
    };
    calculatePrice () {
        let totalSum  = 0;
        totalSum+=this._size.price;
        totalSum+=this._stuffing.price;
        totalSum+=this._toppings.reduce(function (total,item) {
            return total + item.price;
        },0);
        return totalSum;
    };

    calculateCalories () {
    let totalKcal  = 0;
    totalKcal+=this._size.kcal;
    totalKcal+=this._stuffing.kcal
    totalKcal+=this._toppings.reduce(function (total,item) {
        return total + item.kcal;
    },0)
    return totalKcal;
    };
}




class HamburgerException{
    constructor(message){
        this._message = message;
        console.log('HamburgerException message:', message);
    }

}
console.log(Hamburger.prototype);
let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.getToppings());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());
let burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
console.log(burger.getToppings());
console.log(burger.calculatePrice());
console.log(burger.calculateCalories());
